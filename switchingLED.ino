void setup() {
  // put your setup code here, to run once:
  pinMode(0,OUTPUT);
  pinMode(1,OUTPUT);
  pinMode(13,OUTPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  digitalWrite(0,LOW);
  delay(1000);
  digitalWrite(0,HIGH);
  delay(1000);
  digitalWrite(1,LOW);
  delay(2000);
  digitalWrite(1,HIGH);
  delay(2000);
  digitalWrite(13,LOW);
  delay(4000);
  digitalWrite(13,HIGH);
  delay(4000);
}

