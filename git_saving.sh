#!/bin/bash
# Enregistrement GIT

ACTUAL=`pwd`
rm ~/M3206/.comment_sauvegarder_son_espace_git
cp git_saving.sh ~/M3206
cd ~/M3206
rm .comment_sauvegarder_son_espace_git
touch .comment_sauvegarder_son_espace_git
cat git_saving >> .comment_sauvegarder_son_espace_git
for i in `ls -d */`;do cp git_saving.sh $i/;rm .comment_sauvegarder_son_espace_git; cp .comment_sauvegarder_son_espace_git $i/;ls -l git_saving.sh;done
cd $ACTUAL
git status
echo "Enregistrer quel fichier?"
read savefile
echo "Exportation du fichier $savefile en cours ..."
echo "-       -"
echo "-   -   -"
echo " - - - - "
echo "  - - -  "
echo ""
echo "----------------"
git add $savefile > /dev/null
git commit -m "$savefile enregistré" > /dev/null
git push origin master
echo "$savefile est bien exporté."
